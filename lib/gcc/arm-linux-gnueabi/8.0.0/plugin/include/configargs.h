/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --disable-multilib --disable-werror --disable-libgomp --enable-languages=c --target=arm-linux-gnueabi --prefix=/home/rev3nt3ch/B14CKEN3D-TC/build-tools-gcc/arm-linux-gnueabi";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "tls", "gnu" } };
